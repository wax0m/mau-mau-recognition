'''
In this script is implemented function to draw card and returning a random card from deck.
'''


import mau_mau_impl as mmi
import random
import csv

def draw_card_from_deck():
    # pokud uz v balicku nejsou zadne karty pro lizani, tak zamicham balicek
    if mmi.no_card_for_draw():
        mmi.shuffle_cards()
    cards_list = mmi.get_cards(mmi.card_deck_path)
    card_index = random.randrange(0, 31)
    num = cards_list[card_index][1]
    if num != "0":
        found_card = False
        while not found_card:
            card_index = (card_index + 1) % 32
            num = cards_list[card_index][1]
            if num == "0":
                found_card = True
    cards_list[card_index][1] = "1"
    with open(mmi.resource_path(mmi.card_deck_path), 'wb') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(cards_list)
    return cards_list[card_index][0]
