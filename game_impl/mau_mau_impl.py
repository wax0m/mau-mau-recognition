'''
In this script are implemented functions to initialize the game and handle cards and .tsv files.
This is called from the C# project when starting the game.
This script accepts 1 argument which sets number of playing players.
'''


import os
import sys
import csv
from card_recognition.card_recognition import bot_recognize
import random
import glob

# Importuji jednotlive slozky implementace hry
import draw_card as dr
import play_one_turn as pot
from validate_card import validate


BOT_NUMBER = 2          # CHANGE THIS TO YOUR BOT NUMBER:
                        # BOT_NUMBER in (1,5) - DEFAULT BOT_NUMBER = 2 or
                        # BOT_NUMBER not in (1,5) to deactivate bot

current_dir_path = os.path.abspath(os.path.join(os.path.abspath(__file__), '..'))        # appending current_dir_path and ../ to all paths in file

hand_path = os.path.abspath(os.path.join(current_dir_path, "../resources/play/current_hand.tsv"))
card_deck_path = os.path.abspath(os.path.join(current_dir_path, "../resources/cards_deck.tsv"))
top_card_path = os.path.abspath(os.path.join(current_dir_path, "../resources/play/top_card.tsv"))
draw_path = os.path.abspath(os.path.join(current_dir_path, "../resources/play/draw.tsv"))


def init_game(number_of_players):

    if number_of_players > 5 or number_of_players < 2:
        print "Invalid number of players"
        return
    path = get_abs_path_from_project_directory("resources/play/player")
    print path
    for i in range(number_of_players):
        new_path = path + (str(i + 1))
        with open((new_path + ".tsv"), 'wb') as out_file:
            tsv_writer = csv.writer(out_file, delimiter='\t')
            for j in range(4):
                card = dr.draw_card_from_deck()
                # print card
                tsv_writer.writerow([card])
    card = dr.draw_card_from_deck()
    with open((top_card_path), 'wb') as tc_file:
        tsv_writer = csv.writer(tc_file, delimiter='\t')
        list = [card]
        list.append(0)
        tsv_writer.writerow(list)


# nastavi vsechny cisla v cards deck na nulu = tzn. ze nebyly tazeny
def reset_deck():
    cards_list = get_cards(card_deck_path)
    for i in range(len(cards_list)):
        cards_list[i][1] = 0
    with open(resource_path(card_deck_path), 'wb') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(cards_list)


def bot_choose(top_card, bot_name):
    player_path = get_abs_path_from_project_directory("resources/play/" + bot_name)
    new_path = player_path + ".tsv"
    hand = get_cards(new_path)
    print(hand)
    for card in hand:
        if validate(card[0], top_card[0]):
            return card
    return None


# uz neni k dispozici zadna karta pro liznuti
def no_card_for_draw():
    cards = get_cards(card_deck_path)
    for i in range(len(cards)):
        if cards[i][1] == "0":
            return False
    return True


# prepise vsechny na nepouzite, precte karty co maji hraci v ruce
# tyto karty nastavi na jednicku a zamicha karty v card deck
def shuffle_cards():
    cards = get_cards(card_deck_path)
    for i in range(len(cards)):
        cards[i][1] = 0
    files = glob.glob(get_abs_path_from_project_directory("resources/play/player?.tsv"))
    cards_in_hand = []
    for i in range(len(files)):
        cards_player = get_cards(files[i])
        for j in range(len(cards_player)):
            cards_in_hand.append(cards_player[j])
    for i in range(len(cards_in_hand)):
        cards.remove([cards_in_hand[i][0], 0])
        cards.append([cards_in_hand[i][0], 1])
    random.shuffle(cards)
    #zapsat do souboru
    with open(resource_path(card_deck_path), 'wb') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(cards)



# We don't use this
def change_draw(num_cards):
    with open(resource_path(draw_path), 'r+') as out_file:
        list = get_cards(draw_path)
        # kartu ulozim jako prvni (a jediny) prvek listu
        # if used is not None:
        list[0][0] = str(num_cards)
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(list)

def change_topdeck(str_card, used=None):
    with open(resource_path(top_card_path), 'r+') as out_file:

        list = get_cards(top_card_path)
        # kartu ulozim jako prvni (a jediny) prvek listu
        list[0][0] = str_card
        list[0][1] = str(used)
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(list)



def play_turn():
    current_hand = get_cards(card_deck_path)
    top_card = bot_recognize()[0]
    # TODO some game strategizing with current hand and a top_card
    # TODO If bot decides to play then remove card Else draw a new card and store it
    return


def resource_path(relative_path):
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    # print os.path.join(base_path, '..', '..', relative_path)
    return os.path.join(base_path, '..', '..', relative_path)

def get_abs_path_from_project_directory(relative_path):
    project_dir_path = os.path.abspath(os.path.join(os.path.abspath(__file__), '..', '..'))   # absolutni root cesta k projektu
    # print project_dir_path
    return os.path.abspath(os.path.join(project_dir_path, relative_path))

def remove_card(path):
    tsv_data = []
    with open(resource_path(path), 'r') as tsv_in:
        tsv_reader = csv.reader(tsv_in, delimiter='\t')

        for record in tsv_reader:
            tsv_data.append(record)

    tsv_data.pop((len(tsv_data) - 1))
    print(tsv_data)
    # with open(resource_path(path), 'wt') as out_file:       # old
    with open(resource_path(path), 'wb') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(tsv_data)
    return


def get_cards(path):
    tsv_data = []
    with open(get_abs_path_from_project_directory(path), 'r') as tsv_in:
        tsv_reader = csv.reader(tsv_in, delimiter='\t')

        for record in tsv_reader:
            tsv_data.append(record)

    return tsv_data


def store_card():
    card_pair = bot_recognize()[0]
    card = []
    for i in card_pair:
        card.append(i)
    tsv_data = get_cards(hand_path)

    tsv_data.append(card)
    print(tsv_data)
    # with open(resource_path(hand_path), 'wt') as out_file:      # old
    with open(resource_path(hand_path), 'wb') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerows(tsv_data)


def main():
    store_card()
    # remove_card()

def dummy_game_simulation():
    # basic script for testing - call mau mau game
    # for this script num_of_players equals 4
    # each player tries to play with first card

    reset_deck()
    num_of_players = 4
    init_game(num_of_players)

    # dummy game simulation for testing purposes
    print "Na vrchu je karta " + get_cards(top_card_path)[0][0]
    print "Hrac 1 hraje:"
    pot.play_one_turn(1, get_cards(get_abs_path_from_project_directory("resources/play/player1.tsv"))[0][0])
    print "Hrac 2 hraje:"
    pot.play_one_turn(2, get_cards(get_abs_path_from_project_directory("resources/play/player2.tsv"))[0][0])
    print "Hrac 3 hraje:"
    pot.play_one_turn(3, get_cards(get_abs_path_from_project_directory("resources/play/player3.tsv"))[0][0])
    print "Hrac 4 hraje:"
    pot.play_one_turn(4, get_cards(get_abs_path_from_project_directory("resources/play/player4.tsv"))[0][0])


if __name__ == "__main__":
    # if you want to run in pycharm or in terminal, please (un)comment line 13 and edit line 298 to given number or variable
    # you can comment prints in play_one_turn method
    print current_dir_path
    print hand_path
    print top_card_path
    reset_deck()
    # TODO napsat metodu co vynuluje soubory player?.tsv a top_card.tsv aby byly prazdny
    #  a tady ji zavolat
    num = 0
    num_of_players = ""
    if len(sys.argv) >=2:
        num_of_players = int(sys.argv[1])
    if num_of_players == "":
        num = 4
    else:
        num = int(num_of_players)
    # print num
    init_game(num)

# if __name__ == "__main__":
#     dummy_game_simulation()


