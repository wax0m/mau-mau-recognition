'''
In this script is implemented function to make a move.
This is called from the C# project when player is trying to make a move.
This script accepts 1 up to 3 arguments setting current playing number, card and new color (if Queen is played).
This script also implements basic logic of the game.
'''


import csv
import sys

# Importuji jednotlive slozky implementace hry
import mau_mau_impl as mmi
import draw_card as dr


BOT_NUMBER = 2


def play_one_turn(player_number, card=None, new_color=None):
    # print "chce hrat kartou " + card
    # hrajeme bez prebijeni
    player_path = mmi.get_abs_path_from_project_directory("resources/play/player")
    new_path = player_path + str(player_number) + ".tsv"
    top = str(mmi.get_cards(mmi.top_card_path)[0][0])   #karta na vrchu
    values_top = top.split("_")                         #list cislo, barva vrchni karty
    topdeck = mmi.get_cards(mmi.top_card_path)[0]       #list karta, cislo
    # prvni vec je karta string a druha vec je cislo 0 nebo 1
    used_card = "1"
    if len(topdeck) > 1:
        used_card = str(topdeck[1])  # to je to cislo
    if values_top[0] == "7" and used_card == "0":
        # draw 2 cards
        # print "Beru dve"
        with open(mmi.resource_path(new_path), 'r'):
            player_cards = mmi.get_cards(new_path)
        # with open(resource_path(new_path), 'wt') as hand_file:      # old
        with open(mmi.resource_path(new_path), 'wb') as hand_file:
            tsv_writer = csv.writer(hand_file, delimiter='\t')
            new_card = dr.draw_card_from_deck()
            player_cards.append([new_card])
            new_card = dr.draw_card_from_deck()
            player_cards.append([new_card])
            tsv_writer.writerows(player_cards)
            mmi.change_topdeck(top, 1)
        return
    elif values_top[0] == "A" and used_card == "0":
        # print "Stojim"
        mmi.change_topdeck(top, 1)
        return
    else:
        if card is None:
            print "Zadnou kartu nehraju - lizu kartu"
            new_card = dr.draw_card_from_deck()
            with open(mmi.resource_path(new_path), 'r'):
                player_cards = mmi.get_cards(new_path)
            # with open(resource_path(new_path), 'wt') as hand_file:        # old
            with open(mmi.resource_path(new_path), 'wb') as hand_file:
                player_cards.append([new_card])
                tsv_writer = csv.writer(hand_file, delimiter='\t')
                tsv_writer.writerows(player_cards)
            return
        else:
            values_card = card.split("_")
            if values_card[0] == "Q":
                print "Hraju menice"
                # je to menic
                with open(mmi.resource_path(new_path), 'r'):
                    list = mmi.get_cards(new_path)
                    list.remove([card])
                # with open(resource_path(new_path), 'wt') as hand_file:        # old
                with open(mmi.resource_path(new_path), 'wb') as hand_file:
                    tsv_writer = csv.writer(hand_file, delimiter='\t')
                    tsv_writer.writerows(list)
                values_card[1] = new_color
                str_card = str(values_card[0]) + "_" + str(values_card[1])
                mmi.change_topdeck(str_card, 0)
            else:
                # neni to menic
                if values_top[0] == values_card[0] or values_top[1] == values_card[1]:
                    print "Hraju kartu"
                    if values_card[0] == "7" or values_card[0] == "A":
                        mmi.change_topdeck(card, 0)
                    else:
                        mmi.change_topdeck(card, 0)
                    with open(mmi.resource_path(new_path), 'r') as hand_file:
                        list = mmi.get_cards(new_path)
                        list.remove([card])
                    # with open(resource_path(new_path), 'wt') as hand_file:      # old
                    with open(mmi.resource_path(new_path), 'wb') as hand_file:
                        tsv_writer = csv.writer(hand_file, delimiter='\t')
                        tsv_writer.writerows(list)
                else:
                    print "Hraju nevalidni kartou"
    return


if __name__ == "__main__":
    player_number = None
    card = None
    type = None
    if len(sys.argv) == 2:
        player_number = sys.argv[1]
    elif len(sys.argv) == 3:
        player_number = sys.argv[1]
        card = sys.argv[2]
    elif len(sys.argv) == 4:
        player_number = sys.argv[1]
        card = sys.argv[2]
        type = sys.argv[3]

    play_one_turn(player_number, card, type)

