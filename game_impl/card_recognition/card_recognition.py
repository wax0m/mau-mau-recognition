'''
In this script are implemented functions to learn from dataset, compare and return most similar card available from robot's playing hand.
This is called from script play_one_turn_bot.py when bot is playing.
'''


import os
import sys
import numpy as np
import pyautogui
import cv2


sys.path.insert(0, "/usr/local/lib/python2.7/site-packages/")

def resource_path(relative_path):
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, '..', relative_path)


train_png = resource_path("../resources/train/full_layout_black_background_with_grid.png")
train_labels = resource_path("../resources/train/train_new.tsv")
test_imgs = resource_path("../resources/imgs/cards/")


def rectify(h):
    #print(h)
    size = len(h)
    h = h.reshape((size, 2))
    hnew = np.zeros((size, 2), dtype=np.float32)

    add = h.sum(1)
    hnew[0] = h[np.argmin(add)]
    hnew[2] = h[np.argmax(add)]

    diff = np.diff(h, axis=1)
    hnew[1] = h[np.argmin(diff)]
    hnew[3] = h[np.argmax(diff)]

    return hnew


###############################################################################
# Image Matching
###############################################################################
def preprocess(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 2)
    # thresh = cv2.adaptiveThreshold(img, 255, 1, 1, 11, 1)
    return img


def imgdiff(img1, img2):
    img1 = cv2.GaussianBlur(img1, (5, 5), 5)
    img2 = cv2.GaussianBlur(img2, (5, 5), 5)
    diff = cv2.absdiff(img1, img2)
    diff = cv2.GaussianBlur(diff, (5, 5), 5)
    flag, diff = cv2.threshold(diff, 200, 255, cv2.THRESH_BINARY)
    return np.sum(diff)


def find_closest_card(training, img):
    features = preprocess(img)
    closest = sorted(training.values(), key=lambda x: imgdiff(x[1], features))[0][0]
    # print(closest)
    return closest


###############################################################################
# Card Extraction
###############################################################################
def getCards(im, numcards=4):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (1, 1), 1000)
    flag, thresh = cv2.threshold(blur, 120, 255, cv2.THRESH_BINARY)

    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    contours = sorted(contours, key=cv2.contourArea, reverse=True)[:numcards]
    # print(len(contours))
    # cv2.imshow('a', contours[0])
    # cv2.waitKey(0)
    for card in contours:
        peri = cv2.arcLength(card, True)
        #print(len(cv2.approxPolyDP(card, 0.02 * peri, True)))
        approx = rectify(cv2.approxPolyDP(card, 0.02 * peri, True))
        #print("approx: ")
        #print(approx)
        while len(approx) > 4:
            approx = approx[:-1]

        # if len(contours) == 1:
        #     box = np.int0(approx)
        #     cv2.drawContours(im,[box],0,(255,255,0),6)
        #     imx = cv2.resize(im,(1000,600))
        #     cv2.imshow('a',imx)
        #     cv2.waitKey(0)

        h = np.array([[0, 0], [449, 0], [449, 449], [0, 449]], np.float32)

        transform = cv2.getPerspectiveTransform(approx, h)
        warp = cv2.warpPerspective(im, transform, (450, 450))

        yield warp


def get_training(training_labels_filename, training_image_filename, num_training_cards, avoid_cards=None):
    training = {}
    file = open(training_labels_filename)
    labels = {}
    for line in file:
        key, num, suit = line.strip().split()
        labels[int(key)] = (num, suit)

    # print("Training")

    im = cv2.imread(training_image_filename)
    for i, c in enumerate(getCards(im, num_training_cards)):
        if avoid_cards is None or (labels[i][0] not in avoid_cards[0] and labels[i][1] not in avoid_cards[1]):
            training[i] = (labels[i], preprocess(c))
            # cv2.imshow(labels[i], c)
            # cv2.waitKey(0)

    # print("Done training")
    return training

def screenshot():
    image = pyautogui.screenshot()
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    return image

def bot_recognize():
    image = screenshot()
    cards = recog_card(image)
    return cards

def recognize_topdeck(test=None):
    # for testing purposes
    filename = os.path.join(test_imgs, 'gui_test.png')
    #print(filename)
    if test is not None:
        img = cv2.imread(filename)
    else:
        img = screenshot()
    width = img.shape[0]
    height = img.shape[1]
    # Change this to your window resolution - Working on FHD
    crop_height_from = int(2./10. * height)
    crop_height_to = int(3.6/10. * height)
    crop_width_from = int((8./10.) * width)
    # crop_width_from = int((width * 0.5) - 100)
    crop_width_to = int(9.75/10.*width)
    # crop_width_to = int((width * 0.5) + 100)
    img = img[crop_height_from:crop_height_to, crop_width_from:crop_width_to]
    # cv2.imshow("test", img)
    # cv2.waitKey(0)
    #print(img)
    return recog_card(img)

# We don't use this
def recognize_drawn_card(test=None):
    # for testing purposes
    filename = os.path.join(test_imgs, 'gui_test3.png')

    if test is not None:
        img = cv2.imread(filename)
    else:
        img = screenshot()
    width = img.shape[0]
    height = img.shape[1]

    crop_height_from = int(3.5/10. * height)
    crop_height_to = int(10./10. * height)
    crop_width_from = int(0./10. * width)
    crop_width_to = int(3.3/10.*width)
    #print(crop_width_to)
    img = img[crop_height_from:crop_height_to, crop_width_from:crop_width_to]
    # print(img.shape[0])
    # print(img.shape[1])
    # cv2.imshow("test", img)
    # cv2.waitKey(0)
    # print(img)
    return recog_card(img)


def recog_card(test_image=None):
    filename = os.path.join(test_imgs, '6.jpg')
    num_cards = 1
    training_image_filename = train_png
    training_labels_filename = train_labels
    num_training_cards = 32

    training = get_training(training_labels_filename, training_image_filename, num_training_cards)

    if test_image is not None:
        im = test_image
    else:
        im = cv2.imread(filename)

    width = im.shape[0]
    height = im.shape[1]
    # if width < height:
    #     im = cv2.transpose(im)
    #     im = cv2.flip(im, 1)
    # Debug: uncomment to see registered images
    # for i, c in enumerate(getCards(im, num_cards)):
    #     card = find_closest_card(training, c, )
    #     cv2.imshow(str(card), c)
    # cv2.waitKey(0)

    cards = [find_closest_card(training, c) for c in getCards(im, num_cards)]
    #print(cards)
    return cards

if __name__ == '__main__':
    recognize_drawn_card(True)