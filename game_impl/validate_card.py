'''
In this script is implemented function to validate a card given as argument.
This is called from the C# project and from other scripts to validate whether the move player/bot is trying to play is valid.
This script accepts 1 argument that is card we are validating.
'''

import sys

# Importuji jednotlive slozky implementace hry
import mau_mau_impl as mmi


def validate(card, top_card=None):
    if top_card is not None:
        top = top_card
    else:
        top = str(mmi.get_cards(mmi.top_card_path)[0][0])
    values_top = top.split("_")
    values_card = card.split("_")
    if (values_top[0] == "7" or values_top[0] == "A") and values_top[1] == "1":
        return False
    elif values_card[0] == "Q":
        return True
    elif values_top[0] == values_card[0] or values_top[1] == values_card[1]:
        return True
    return False


if __name__ == "__main__":
    card = None
    top_card = None
    if len(sys.argv) == 2:
        card = sys.argv[1]
    elif len(sys.argv) == 3:
        card = sys.argv[1]
        top_card = sys.argv[2]

    print validate(card, top_card)
