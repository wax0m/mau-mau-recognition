# MauMau Card Game with Visual Recognition

MauMau card game visual recognition

___
## Credits & License

**Developers**

`Aleš Král`
* GitLab: [kralale4](https://gitlab.fel.cvut.cz/kralale4)
* email: kralale4@fel.cvut.cz

`Adéla Kubíková`
* GitLab: [kubikade](https://gitlab.fel.cvut.cz/kubikade)
* email: kubikade@fel.cvut.cz

`Hung Pham`
* GitLab: [phamnhun](https://gitlab.fel.cvut.cz/phamnhun)
* email: phamhun@fel.cvut.cz


If you would like to use this program, please contact us.  
For major changes, please open an issue first to discuss what you would like to change.

___
## Introduction
This project was created as combination of projects on subjects A6M33KSY and B4B39IUR. <br/>
The project in an implementation of the windowed game of Mau-mau and the robot able to play the game
by visual recognition of the last played card by using library OpenCV-Python. 
The robot's logic is very basic and tries to play first available card in its hand.
The game is in local multiplayer (2-5 players) where you have to pass the device, where
you keep the program on between players. 

___
## Platform, Compilation & Requirements

This program is implemented in C# and Python 2.7. 
C# solution is using WPF (.NET framework) - this means that the program is going to work only on Windows platform.
Before building and executing the program in Visual Studio, please complete the following steps first.

**Please make sure you fulfill all the requirements and steps below
otherwise this project will not work for you.**

### Requirements
- Windows OS
- Visual Studio (any version)
- Python 2.7 and pip2
- Works best on FullHD monitor


### Compilation

***1. Please install following packages to Python 2.7:***

- PyAutoGUI 
```bash 
pip2 install pyautogui
```
- Image
```bash 
pip2 install Image
```
- CSV
```bash 
pip2 install csv
```
- NumPy
```bash 
pip2 install numpy==1.15.0
```
- OpenCV-Python
```bash
pip2 install opencv-python==4.2.0.32
```


***2. Change path to your python.exe file:***

In file `/hw07_maumau/Support/PythonSupport.cs` change <br/>
`psi.FileName=@"MY_PATH"` to `psi.FileName=@"YOUR_PATH"` 
according to your path to your **python.exe** file.

___
## Libraries

```python
import cv2
```
Library `OpenCV-Python(cv2)` is used for card recognition 
and comparing between taken screenshot and the learned card data.
___
```python
import numpy
```
Library `NumPy` is used for most of the mathematical operations.
___
```python
import pyautogui
```
Library `PyAutoGUI` is used for taking screenshots of the game/GUI 
which are later used for comparing with robot's learned data.
___
```python
import csv
```
Library `CSV` is used for workflow with .tsv files.
___

## Manual

The implementation of the game's logic is very basic. 
If player before you played card with number `7` or `A` you have to follow the basic rules of Mau-Mau game. <br/> 
You can not play the `7` or `A` in order to counter the played card before you.

***1. Step:*** <br/>
Before first start of the application locate file `/game_impl/mau_mau_impl.py` and change the number in variable 
`BOT_NUMBER = {player_#_is_a_bot}`.

***2. Step:*** <br/>
Open file `/hw07_maumau.sln` in Visual Studio

***3. Step:*** <br/>
Build the solution and start the application. 

***4. Step*** <br/>
The window with drop-down selection and 2 buttons will appear. 
Chose maximum player count in drop-down selection and start the actual game with a `Start Game` button.

***5. Step*** <br/>
Play the game...
Bot is Player `{x}` according to the `BOT_NUMBER` you have set in ***[1. Step]***.

***6. Step:*** <br/>
To exit the game you can finish it by winning, or press the button `Menu` and exit to `Main Window` or quit to desktop.

### In Game Controls
- **`Take Turn`** - button to take turn. Reveals cards of the current playing player.
- **`Pass Turn`** - button to pass turn. Pass the turn after your move. 
If you can not play because of played card `A` before you press this button to pass the turn to next player.
- **`Draw Card`** - button to draw card. If you have no cards to play, or you want to draw a card. 
- **`Last Played Card`** - last played card is placed in the middle of the screen.
- **`Last Drawn Card`** - last drawn card is placed in the left bottom corner.
- **`Current Player's Hand`** - current player's cards are shown in bottom of the screen.
- **`Show More`** - button to show more cards. This might be useful when player has more cards than can be shown
in the bottom row.
- **`Menu`** - button to open menu.
- **`All player's status`** - list of players and the statuses of their hands shown in the left upper corner 
below `Menu` button.
- **`Player {x} is playing`** - indicator of the current playing player. 
If bot takes the turn there will be additional textbox shown.
___
***Our robot can play and draw cards by it own. 
However, it still needs human's input such as passing and taking turns. <br/>
This is set that way, so you can check the move robot tried to make.***

![Player view](/resources/imgs/quick_view_player.png)

![Robot view](/resources/imgs/quick_view_robot.png)
- Screenshot of the actual game when robot is playing (highlighted important features)

 
**Thank you!**

