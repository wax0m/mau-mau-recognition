﻿using hw07_maumau.Support;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace hw07_maumau.Controls
{
    public class PlayerCardCustomControl : Button
    {
        static PlayerCardCustomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PlayerCardCustomControl), 
                new FrameworkPropertyMetadata(typeof(PlayerCardCustomControl)));
        }

        public string Card
        {
            get
            {
                return (string)GetValue(CardProperty);
            }
            set
            {
                SetValue(CardProperty, value);
            }
        }
        
        public string Number
        {
            get
            {
                return (string)GetValue(CardProperty);
            }
            set
            {
                SetValue(CardProperty, value);
            }
        }

        public string Type
        {
            get
            {
                return (string)GetValue(TypeProperty);
            }
            set
            {
                SetValue(TypeProperty, value);
            }
        }

        public string Img
        {
            get
            {
                return (string)GetValue(ImgProperty);
            }
            set
            {
                SetValue(ImgProperty, value);
            }
        }

        public RelayCommand Play
        {
            get 
            { 
                return (RelayCommand)GetValue(PlayProperty); 
            }
            set 
            { 
                SetValue(PlayProperty, value); 
            }
        }


        public static readonly DependencyProperty CardProperty =
             DependencyProperty.Register("Card", typeof(string), typeof(PlayerCardCustomControl));

        public static readonly DependencyProperty NumberProperty =
             DependencyProperty.Register("Number", typeof(string), typeof(PlayerCardCustomControl));

        public static readonly DependencyProperty TypeProperty =
             DependencyProperty.Register("Type", typeof(string), typeof(PlayerCardCustomControl));

        public static readonly DependencyProperty ImgProperty =
             DependencyProperty.Register("Img", typeof(string), typeof(PlayerCardCustomControl));
        
        public static readonly DependencyProperty PlayProperty =
             DependencyProperty.Register("Play", typeof(string), typeof(PlayerCardCustomControl));
    }
}
