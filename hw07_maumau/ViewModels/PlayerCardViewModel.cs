﻿using hw07_maumau.Support;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace hw07_maumau.ViewModels
{
    public class PlayerCardViewModel : ViewModelBase
    {
        public PlayerCardViewModel()
        {
        }

        public PlayerCardViewModel(MainGameViewModel mainGameReference, string card)
        {
            _cardInput = card;
            _cardImage = card;
            var token = card.Split('_');
            _cardNumber = token[0];
            _cardType = token[1];

            _playable = true;

            _mainGame = mainGameReference;
            _processStartInfo = pythonHandler.CreatePythonProcessStartInfo();

        }
        //================================
        // Important Variables
        private string PROJECT_PATH = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        //================================

        private MainGameViewModel _mainGame;

        //================================
        // Python implementation
        private PythonHandler pythonHandler = new PythonHandler();
        private PythonResultParser _pythonResultParser = new PythonResultParser();
        private ProcessStartInfo _processStartInfo;
        //================================

        private RelayCommand _playCardCommand;

        private string _cardInput;
        private string _cardNumber;
        private string _cardType;
        private string _cardImage;

        private bool _playable;


        public string CardInput { get { return _cardInput; } set { _cardInput = value; OnPropertyChanged(); } }
        public string CardNumber { get { return _cardNumber; } set { _cardNumber = value; OnPropertyChanged(); } }
        public string CardType { get { return _cardType; } set { _cardType = value; OnPropertyChanged(); } }
        public string ImagePath { get { return _cardImage; } set { _cardImage = value; OnPropertyChanged(); } }
        public bool Playable { get { return _playable; } set { _playable = value; OnPropertyChanged(); } }

        public RelayCommand PlayCard { get { return _playCardCommand ?? (_playCardCommand = new RelayCommand(PlayTheCard, PlayCanExecute)); } }
        

        private void Nothing(object obj) { }
        private void PlayTheCard(object obj)
        {
            // call python script to play the card
            // update current players hand via python
            
            

            // validate if player is trying to play the Queen
            // if _cardNumber = Queen, open popup window and do not continue here
            // else continue with the code below

            if (_cardNumber == "Q")
            {
                _mainGame.NEW_COLOR = _cardInput;

                if (_mainGame.SHOW_MORE)
                    _mainGame.SHOW_MORE = false;
                _mainGame.COLOR_SELECTION_POPUP_WINDOW = true;
            }
            else
            {
                var scriptPath = Path.Combine(PROJECT_PATH, "game_impl/play_one_turn.py");
                _processStartInfo.Arguments = $"\"{scriptPath}\" \"{_mainGame.CurrentPlayingPlayer}\" \"{this._cardInput}\"";
                var errors = "";
                var results = "";
                using (var process = Process.Start(_processStartInfo))
                {
                    errors = process.StandardError.ReadToEnd();
                    results = process.StandardOutput.ReadToEnd();
                };

                _mainGame.PLAYER_PLAYED = true;
                _mainGame.Update();
            }
        }

        private bool PlayCanExecute(object obj)
        {
            if (!Playable || _mainGame.PLAYER_PLAYED || _mainGame.LEFT_TO_DRAW > 0)
            {
                return false;
            }
            else
            {
                var scriptPath = Path.Combine(PROJECT_PATH, "game_impl/validate_card.py");
                _processStartInfo.Arguments = $"\"{scriptPath}\" \"{this._cardInput}\"";
                var errors = "";
                var results = "";
                using (var process = Process.Start(_processStartInfo))
                {
                    errors = process.StandardError.ReadToEnd();
                    results = process.StandardOutput.ReadToEnd();
                    errors = _pythonResultParser.ParseToString(errors);
                    var resultsBool = _pythonResultParser.ParseToBool(results);
                    return resultsBool;
                };
            }
            
            return false;
        }
    }
}
