﻿using hw07_maumau.Support;
using hw07_maumau.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace hw07_maumau.ViewModels
{
    public class MainMenu : ViewModelBase
    {
        public MainMenu(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            for (int i = MIN_PLAYERS; i <= MAX_PLAYERS; i++)
            {
                _playerCountCB.Add(i + " Players");
            }
            // Create Process Info and setup python executable
            PROJECT_PATH = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            _processStartInfo = pythonHandler.CreatePythonProcessStartInfo();

        }

        //================================
        // Important Variables
        private string PROJECT_PATH;
        private int MIN_PLAYERS = 2;
        private int MAX_PLAYERS = 5;

        //================================

        private Window _mainWindow;
        private GameWindow _gameWindow;

        // Python implementation
        private PythonHandler pythonHandler = new PythonHandler();
        private ProcessStartInfo _processStartInfo;

        private ObservableCollection<string> _playerCountCB = new ObservableCollection<string>();
        private string _selectedNumOfPlayers;


        private RelayCommand _createNewGameCommand;
        private RelayCommand _closeWindowCommand;

        public ObservableCollection<string> PlayerCountCB
        {
            get { return _playerCountCB; }
            set
            {
                _playerCountCB = value;
                OnPropertyChanged();
            }
        }

        public string SelectedNumOfPlayers
        {
            get { return _selectedNumOfPlayers; }
            set
            {
                _selectedNumOfPlayers = value;
                OnPropertyChanged(nameof(SelectedNumOfPlayers));
            }
        }
        

        public RelayCommand CreateNewGame
        {
            get { return _createNewGameCommand ?? (_createNewGameCommand = new RelayCommand(CreateNewWindow, IsSelectedNumOfPlayers)); }
        }

        public RelayCommand CloseWindow
        {
            get { return _closeWindowCommand ?? (_closeWindowCommand = new RelayCommand(CloseThisWindow)); }
        }

        public void CloseThisWindow(object obj)
        {
            if(_mainWindow != null)
                _mainWindow.Close();
            if (_gameWindow != null)
                _gameWindow.Close();
            _mainWindow = null;
            _gameWindow = null;
        }

        public void CloseGameWindow()
        {
            if (_gameWindow != null)
                _gameWindow.Close();
            _gameWindow = null;
        }

        private void CreateNewWindow(object obj)
        {
            int numOfPlayers = int.Parse(StripStringFromCount(obj.ToString()));
            // call python script to init game
            var scriptPath = Path.Combine(PROJECT_PATH, "game_impl/mau_mau_impl.py");
            _processStartInfo.Arguments = $"\"{scriptPath}\" \"{numOfPlayers}\"";
            
            // Calling python script to initialize game with argument of playing players
            var errors = "";
            var results = "";
            PythonResultParser prp = new PythonResultParser();
            using (var process = Process.Start(_processStartInfo))
            {
                errors = process.StandardError.ReadToEnd();
                results = process.StandardOutput.ReadToEnd();
                errors = prp.ParseToString(errors);
                var result = prp.ParseToString(results);
            }


            _gameWindow = new GameWindow(this, numOfPlayers);
            _gameWindow.Show();             
        }

        private bool IsSelectedNumOfPlayers(object obj)
        {
            int numOfPlayers = 0;
            if(obj != null)
            {
                string stripped = obj.ToString().Split(' ')[0];
                numOfPlayers = int.Parse(StripStringFromCount(obj.ToString()));
            }
                
            if (numOfPlayers != 0 && _gameWindow == null)
            {
                return true;
            }
            else
                return false;
        }
        
        private string StripStringFromCount(string input)
        {
            string count = input.Split(' ')[0];
            return count;
        }
    }
}
