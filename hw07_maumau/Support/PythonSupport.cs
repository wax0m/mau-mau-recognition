﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw07_maumau.Support
{
    public class PythonResultParser
    {
        public PythonResultParser()
        {
        }
        
        public string ParseToString(string input)
        {
            string result = "";
            if (input != null && input.Length >= 2)
                result = input.Substring(0, input.Length - 2);
            return result;
        }

        public int ParseToInt(string input)
        {
            int result = -1;
            if (input != null && input.Length >= 2)
                result = int.Parse(input.Substring(0, input.Length - 2));
            return result;
        }

        public bool ParseToBool(string input)
        {
            bool result = false;
            if (input != null && input.Length >= 2)
                input = input.Substring(0, input.Length - 2);
            if (input.ToLower() == "true")
                result = true;

            return result;
        }
    }

    public class PythonHandler
    {
        public PythonHandler()
        {
        }
        public ProcessStartInfo CreatePythonProcessStartInfo()
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            // Change this path to your python.exe - MUST INSTALL PACKAGES TO BE ABLE TO RUN THE GAME
            psi.FileName = @"C:\Python27\python.exe";                                           
            //psi.FileName = @"C:\Users\WAXOM\AppData\Local\Programs\Python\Python39\python.exe";
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            return psi;
        }
    }
}
